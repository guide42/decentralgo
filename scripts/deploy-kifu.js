const { uploaders } = require("../secrets.json")

async function main() {
  const Kifu = await hre.ethers.getContractFactory("Kifu")
  const kifu = await Kifu.deploy(
    uploaders.map(
      uploader => hre.ethers.Wallet.fromMnemonic(uploader.mnemonic).address
    )
  )

  await kifu.deployed()

  console.info(`Kifu Contract Address = ${kifu.address}`)
}

main().then(() => {
  process.exit(0)
}).catch(err => {
  console.error(err)
  process.exit(1)
})
