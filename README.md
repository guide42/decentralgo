# DecentralGo

Go, Baduk, Weiqi is an ancient game.

## Development

```sh
# Create your secrets
$ cp secrets.example.json secrets.json

# Run node
$ yarn hardhat node

# Deploy
$ yarn hardhat run --network localhost scripts/deploy-kifu.js

# Replace contracts addresses
$ cp contracts.example.json contracts.json

# Test
$ yarn hardhat test

# Run uploaders
$ yarn pm2 start --env development --watch upload uploaders.config.js

# Run app
$ yarn rollup --config --watch --environment APP_ENV:development
```
