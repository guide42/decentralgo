require("@nomiclabs/hardhat-ethers")
require("@nomiclabs/hardhat-waffle")
require("@nomiclabs/hardhat-etherscan")
require("hardhat-abi-exporter")

/**
 * @type import('hardhat/config').HardhatUserConfig
 */
const config = {
  solidity: "0.8.4",
  settings: {
    outputSelection: {
      "*": {
        "*": ["storageLayout"]
      }
    }
  },
  abiExporter: {
    flat: true,
    only: ["DecentralGo", "Kifu"]
  }
}

if (require("fs").existsSync("./secrets.json")) {
  const {
    infuraId,
    alchemyKey,
    etherscanApiKey,
    deployer
  } = require("./secrets.json")

  module.exports = {
    ...config,
    networks: {
      ropsten: {
        url: `https://ropsten.infura.io/v3/${infuraId}`,
        accounts: deployer
      },
      hardhat: {
        forking: {
          url: `https://eth-ropsten.alchemyapi.io/v2/${alchemyKey}`,
          blockNumber: 11458021
        },
        accounts: deployer
      }
    },
    etherscan: {
      apiKey: etherscanApiKey
    }
  }
} else {
  module.exports = {
    ...config
  }
}
