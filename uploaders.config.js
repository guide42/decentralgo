const { uploaders } = require("./secrets.json")

const apps = uploaders.map(
  (uploader, index) => ({
    name: `uploader_${index}`,
    script: `./upload/main.js`,
    args: `--uploader=${index}`,
    watch: process.env.NODE_ENV === "development",
    env_production: {
      NODE_ENV: "production"
    },
    env_development: {
      NODE_ENV: "development"
    }
  })
)

module.exports = { apps }
