import { Interface } from "ethers/lib.esm/utils"

import KifuAbi from "../abi/Kifu.json"

export const Kifu = new Interface(KifuAbi)
