import App from "./App.svelte"
import contracts from "../contracts.json"

export default new App({
  target: document.body,
  props: {
    kifuContracts: contracts["Kifu"]
  }
})
