// SPDX-License-Identifier: ISC
pragma solidity ^0.8.4;
pragma abicoder v2; // required to accept structs as function parameters

import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Enumerable.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/utils/cryptography/ECDSA.sol";
import "@openzeppelin/contracts/utils/cryptography/draft-EIP712.sol";

contract Kifu is ERC721Enumerable, ERC721URIStorage, EIP712, AccessControl {
  using Counters for Counters.Counter;

  Counters.Counter private _tokenIds;

  bytes32 public constant BURNER_ROLE = keccak256("BURNER_ROLE");
  bytes32 public constant MINTER_ROLE = keccak256("MINTER_ROLE");
  bytes32 public constant UPLOADER_ROLE = keccak256("UPLOADER_ROLE");

  // Stored on-chain game data.
  struct Game {
    string black;
    string white;
    string result;
  }

  mapping(uint256 => Game) private _games;

  // Represents an un-minted NFT, which has not yet been recorded into the
  // blockchain.
  struct KifuUploaded {
    string black;
    string white;
    string result;

    // The metadata URI to associate with this token.
    string uri;

    // the EIP-712 signature of all other fields in the KifuUploaded struct.
    bytes signature;
  }

  constructor(address[] memory uploaders)
    ERC721("DecentralGo Kifu", "goKIFU")
    EIP712("goKIFU", "1")
  {
    _setupRole(DEFAULT_ADMIN_ROLE, _msgSender());

    _setupRole(BURNER_ROLE, _msgSender());
    _setupRole(MINTER_ROLE, _msgSender());

    for (uint i = 0; i < uploaders.length; i++) {
      _setupRole(UPLOADER_ROLE, uploaders[i]);
    }
  }

  function game(uint256 tokenId) public view virtual returns (Game memory) {
    return _games[tokenId];
  }

  function _setGame(
    uint256 tokenId,
    string memory black,
    string memory white,
    string memory result
  ) internal virtual {
    _games[tokenId] = Game(black, white, result);
  }

  function burn(uint256[] memory tokenIds) public {
    require(hasRole(BURNER_ROLE, _msgSender()), "Kifu.burn: only burner");
    require(tokenIds.length > 0, "Kifu.burn: at least one tokenId");

    for (uint i = 0; i < tokenIds.length; i++) {
      _burn(tokenIds[i]);
    }
  }

  function inscribeFor(
    address to,
    string memory black,
    string memory white,
    string memory result,
    string memory uri
  ) public returns (uint256) {
    require(hasRole(MINTER_ROLE, _msgSender()), "Kifu.inscribeTo: only minter");

    _tokenIds.increment();
    uint256 newKifuId = _tokenIds.current();

    _mint(to, newKifuId);
    _setGame(newKifuId, black, white, result);
    _setTokenURI(newKifuId, uri);

    return newKifuId;
  }

  function inscribe(KifuUploaded calldata kifu) public returns (uint256) {
    // make sure signature is valid and get the address of the signer
    address signer = _verify(kifu);

    // make sure that the signer is authorized to mint NFTs
    require(
      hasRole(UPLOADER_ROLE, signer),
      "Kifu.inscribe: Signature invalid or unauthorized"
    );

    _tokenIds.increment();
    uint256 newKifuId = _tokenIds.current();

    _mint(_msgSender(), newKifuId);
    _setGame(newKifuId, kifu.black, kifu.white, kifu.result);
    _setTokenURI(newKifuId, kifu.uri);

    return newKifuId;
  }

  // Verifies the signature for a given KifuUploaded.
  function _verify(KifuUploaded calldata kifu) internal view returns (address) {
    bytes32 digest = _hash(kifu);
    return ECDSA.recover(digest, kifu.signature);
  }

  // Returns a hash of the given KifuUploaded, prepared using EIP712 typed data
  // hashing rules.
  function _hash(KifuUploaded calldata kifu) internal view returns (bytes32) {
    return _hashTypedDataV4(keccak256(abi.encode(
      keccak256("KifuUploaded(string black,string white,string result,string uri)"),
      keccak256(bytes(kifu.black)),
      keccak256(bytes(kifu.white)),
      keccak256(bytes(kifu.result)),
      keccak256(bytes(kifu.uri))
    )));
  }

  function tokenURI(uint256 tokenId) public view virtual
    override(ERC721, ERC721URIStorage)
    returns (string memory)
  {
    return super.tokenURI(tokenId);
  }

  function supportsInterface(bytes4 interfaceId) public view virtual
    override(AccessControl, ERC721, ERC721Enumerable)
    returns (bool)
  {
    return super.supportsInterface(interfaceId);
  }

  function _beforeTokenTransfer(address from, address to, uint256 tokenId)
    internal virtual
    override(ERC721, ERC721Enumerable)
  {
    super._beforeTokenTransfer(from, to, tokenId);
  }

  function _burn(uint256 tokenId) internal virtual
    override(ERC721, ERC721URIStorage)
  {
    super._burn(tokenId);
  }
}
