const minimist = require('minimist')
const createServer = require("./server")
const { Wallet } = require("@ethersproject/wallet")
const { uploaders } = require("../secrets.json")

async function main(args) {
  const argv = minimist(args)

  if (!('uploader' in argv) || !uploaders[argv.uploader]) {
    throw new Error(`Not an uploader in arguments`)
  }

  const uploader = uploaders[argv.uploader]
  const port = 'port' in argv ? argv.port : 8000 + argv.uploader

  const wallet = Wallet.fromMnemonic(uploader.mnemonic)
  const server = createServer(wallet)

  server.listen(port, () => {
    console.info(`Server running for uploader ${wallet.address} on ${port}`)
  })
}

main(process.argv).then(() => {}).catch(err => {
  console.error(err)
  process.exit(1)
})
