const jpeg = require("jpeg-js")
const { BOARD_BGCOLOR } = require("./constants")

module.exports = function createBoardJPEG(board) {
  const width = 304
  const height = 304

  const data = Buffer.alloc(width * height * 4)

  function drawPixel(x, y, sign) {
    let i = (y * width + x) * 4

    if (sign === -1) {
      data[i++] = 0xff
      data[i++] = 0xff
      data[i++] = 0xff
      data[i++] = 0xff
    } else if (sign === 1) {
      data[i++] = 0x00
      data[i++] = 0x00
      data[i++] = 0x00
      data[i++] = 0x00
    }
  }

  // draw background
  for (let y = 0; y < height; y++) {
    for (let x = 0; x < width; x++) {
      let i = (y * width + x) * 4
      if ((y - 8) % 16 === 0 && x >= 8 && x <= width - 8) {
        data[i++] = 0x00
        data[i++] = 0x00
        data[i++] = 0x00
      } else if ((x - 8) % 16 === 0 && y >= 8 && y <= height - 8) {
        data[i++] = 0x00
        data[i++] = 0x00
        data[i++] = 0x00
      } else {
        data[i++] = BOARD_BGCOLOR[0]
        data[i++] = BOARD_BGCOLOR[1]
        data[i++] = BOARD_BGCOLOR[2]
      }
      data[i++] = 0xff
    }
  }

  // draw stones
  for (let bx = 0; bx < board.width; bx++) {
    for (let by = 0; by < board.height; by++) {
      if (board.has([bx, by])) {
        const sign = board.get([bx, by])

        const x = bx * 16 + 8
        const y = by * 16 + 8

        let pd = Math.round(Math.PI - (2 * 7))
        let px = 0
        let py = 7

        while (px <= py) {
          drawPixel(x+px, y-py, sign)
          drawPixel(x+py, y-px, sign)
          drawPixel(x+py, y+px, sign)
          drawPixel(x+px, y+py, sign)
          drawPixel(x-px, y+py, sign)
          drawPixel(x-py, y+px, sign)
          drawPixel(x-py, y-px, sign)
          drawPixel(x-px, y-py, sign)

          if (pd < 0) {
            pd = pd + (Math.PI * px) + (Math.PI * 2)
          } else {
            pd = pd + Math.PI * (px - py) + (Math.PI * 3)
            py--
          }

          px++
        }
      }
    }
  }

  return jpeg.encode({ width, height, data }, 90)
}
