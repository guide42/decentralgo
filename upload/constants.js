module.exports = {
  SGF_MIMETYPE: "application/x-go-sgf",
  UPLOAD_TIMEOUT: 60000,
  BOARD_BGCOLOR: [219, 179, 92],
}
