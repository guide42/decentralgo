const smartgame = require("smartgame")
const smartgamer = require("smartgamer")
const GoBoard = require("@sabaki/go-board")

const ALPHA = "ABCDEFGHJKLMNOPQRSTUVWXYZ"

function parseNodeToVertex(node) {
  if (node.length === 2) {
    return [
      ALPHA.indexOf(node[0].toUpperCase()),
      ALPHA.indexOf(node[1].toUpperCase())
    ]
  }

  return [-1, -1]
}

function createBoard(sgf, length) {
  const gamer = smartgamer(sgf)

  let board = new GoBoard(
    Array.from({ length }, _ => Array.from({ length }).fill(0))
  )

  for (let i = 0; i < gamer.totalMoves(); i++) {
    gamer.next()

    const node = gamer.node()

    let sign = 0
    let vertex = [-1, -1]

    if ("B" in node) {
      sign = 1
      vertex = parseNodeToVertex(node.B)
    } else if ("W" in node) {
      sign = -1
      vertex = parseNodeToVertex(node.W)
    }

    if (sign !== 0) {
      board = board.makeMove(sign, vertex, {
        preventSuicide: false,
        preventOverwrite: false,
        preventKo: false,
      })
    }
  }

  return board
}

function getBlack(data) {
  if (data.PB) {
    if (data.BR) {
      return `${data.PB} (${data.BR})`
    }
    return data.PB
  }
  if (data.BT) {
    if (data.BR) {
      return `${data.BT} (${data.BR})`
    }
    return data.BT
  }
  return "-"
}

function getWhite(data) {
  if (data.PW) {
    if (data.WR) {
      return `${data.PW} (${data.WR})`
    }
    return data.PW
  }
  if (data.WT) {
    if (data.WR) {
      return `${data.WT} (${data.WR})`
    }
    return data.WT
  }
  return "-"
}

module.exports = function parseSGF(file) {
  const sgf = smartgame.parse(file)
  const data = sgf.gameTrees.reduce((p, gt) => ({ ...p, ...gt.nodes[0] }), {})

  const black = getBlack(data)
  const white = getWhite(data)
  const result = data.RE ?? "-"
  const size = data.SZ

  const board = createBoard(sgf, size)

  return {
    sgf,
    board,
    metadata: {
      black,
      white,
      result,
      size,
      name: `${black} vs ${white}` + (data.DT ? ` (${data.DT})` : ""),
      description:
        `Go/Baduk/Weiqi game player between ${black} and ${white} on ${data.DT} that ended on ${result}`
    }
  }
}
