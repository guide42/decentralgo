const { Kifu } = require("../contracts.json")

module.exports = async function signKifu(chainId, signer, data) {
  const domain = {
    name: "goKIFU",
    version: "1",
    chainId: parseInt(chainId, 16),
    verifyingContract: Kifu[chainId]
  }

  const types = {
    "KifuUploaded": [
      { name: 'black', type: 'string' },
      { name: 'white', type: 'string' },
      { name: 'result', type: 'string' },
      { name: 'uri', type: 'string' }
    ]
  }

  return await signer._signTypedData(domain, types, data)
}
