const http = require("http")
const connect = require("connect")
const cors = require("cors")
const multer = require("multer")
const createError = require("http-errors")
const { SGF_MIMETYPE, UPLOAD_TIMEOUT } = require("./constants")
const parseSGF = require("./sgf")
const createBoardJPEG = require("./render")
const createNFT = require("./nft")
const signKifu = require("./kifu")

module.exports = function createServer(wallet) {
  const app = connect()
  const upload = multer({
    storage: multer.memoryStorage(),
    limits: {
      fields: 1000,
      files: 10,
      fileSize: 16000000,
    }
  })

  // anyone can upload
  app.use(cors())

  // by POSTing
  app.use(function(req, res, next) {
    if (req.method === "POST") {
      next()
    } else {
      next(createError(405, "Upload SGF using POST method"))
    }
  })

  // an SGF file multipart
  // or the file directly as content-type
  app.use(upload.single("sgf"))
  app.use(function(req, res, next) {
    if (
      req.file &&
      req.file.buffer instanceof Buffer &&
      req.file.mimetype === SGF_MIMETYPE
    ) {
      req.upload = req.file.buffer
      next()
    } else if (req.headers['content-type'] === SGF_MIMETYPE) {
      let body = Buffer.alloc(0)
      let tid = setTimeout(
        () => {
          if (tid) {
            tid = undefined
            next(createError(408, "Upload timeout"))
          }
        },
        UPLOAD_TIMEOUT
      )

      req.on("data", chunk => {
        body = Buffer.concat([body, chunk])
      })

      req.on("end", () => {
        if (tid) {
          tid = clearTimeout(tid)
          req.upload = body
          next()
        }
      })

      req.on("error", err => {
        if (tid) {
          tid = clearTimeout(tid)
          next(err)
        } else {
          console.error(err)
        }
      })
    } else {
      next(createError(400, "Uploaded file is not a valid SGF"))
    }
  })

  app.use(function(req, res) {
    const { board, metadata } = parseSGF(req.upload.toString())

    if (!board.isValid()) {
      throw createError(400, "SGF is not a valid Go/Baduk/Weiqi game")
    }

    const jpeg = createBoardJPEG(board)

    createNFT(
      req.upload,
      jpeg.data,
      metadata
    )
    .then(nft => {
      signKifu(
        "0x7a69",
        wallet,
        {
          black: metadata.black,
          white: metadata.white,
          result: metadata.result,
          uri: nft.url
        }
      )
      .then(signature => {
        res.writeHead(201, { "Content-Type": "application/json" })
        res.end(JSON.stringify({
          black: metadata.black,
          white: metadata.white,
          result: metadata.result,
          uri: nft.url,
          signature
        }))
      })
      .catch(err => {
        throw createError(500, `Signing failed: ${err.message}`)
      })
    })
    .catch(err => {
      throw createError(500, `Upload failed: ${err.message}`)
    })
  })

  app.use(function onerror(err, req, res, next) {
    res.writeHead(err.status ?? 500, { "Content-Type": "application/json" })
    res.end(JSON.stringify({ error: err.message }))
  })

  return http.createServer(app)
}
