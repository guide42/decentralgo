const { NFTStorage, File } = require("nft.storage")
const { nftStorageApiKey } = require("../secrets.json")
const { SGF_MIMETYPE } = require("./constants")

const nftStorage = new NFTStorage({ token: nftStorageApiKey })

module.exports = async function createNFT(sgf, jpeg, metadata) {
  if ("name" in metadata && "description" in metadata) {
    const nft = await nftStorage.store({
      ...metadata,
      image: new File([jpeg], "kifu.jpg", { type: "image/jpg" }),
      sgf: new File([sgf], "kifu.sgf", { type: SGF_MIMETYPE })
    })

    return nft
  }

  throw new Error("Missing name and/or description on metadata")
}
